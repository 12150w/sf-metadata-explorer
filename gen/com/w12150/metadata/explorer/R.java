/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.w12150.metadata.explorer;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
        public static final int ic_launcher_96=0x7f020001;
    }
    public static final class id {
        public static final int add_credentail=0x7f080011;
        public static final int client_list_list=0x7f080002;
        public static final int client_settings=0x7f080012;
        public static final int create_object_cancel=0x7f080005;
        public static final int create_object_create=0x7f080004;
        public static final int create_object_holder=0x7f080003;
        public static final int cred_item_details=0x7f080007;
        public static final int cred_item_name=0x7f080006;
        public static final int field_details_text=0x7f080008;
        public static final int line_lookup_button=0x7f08000a;
        public static final int line_lookup_text=0x7f080009;
        public static final int menu_logout=0x7f080016;
        public static final int menu_query=0x7f080017;
        public static final int menu_settings=0x7f080015;
        public static final int new_client_is_sandbox=0x7f080001;
        public static final int new_client_uname=0x7f080000;
        public static final int object_detail_create=0x7f080014;
        public static final int object_detail_details=0x7f080013;
        public static final int object_detail_list=0x7f08000b;
        public static final int overview_base=0x7f08000c;
        public static final int overview_list_item_label=0x7f080010;
        public static final int overview_list_item_name=0x7f08000e;
        public static final int overview_list_item_prefix=0x7f08000f;
        public static final int overview_login=0x7f08000d;
    }
    public static final class layout {
        public static final int add_client_view=0x7f030000;
        public static final int apex_code_view=0x7f030001;
        public static final int client_list=0x7f030002;
        public static final int create_object=0x7f030003;
        public static final int credential_list_item=0x7f030004;
        public static final int field_details=0x7f030005;
        public static final int lookup_line_layout=0x7f030006;
        public static final int object_detail=0x7f030007;
        public static final int overview=0x7f030008;
        public static final int overview_list_item=0x7f030009;
        public static final int soql_create=0x7f03000a;
    }
    public static final class menu {
        public static final int client_list=0x7f070000;
        public static final int object_detail=0x7f070001;
        public static final int overview=0x7f070002;
    }
    public static final class string {
        public static final int app_name=0x7f050000;
        public static final int hello_world=0x7f050001;
        public static final int menu_settings=0x7f050002;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
    public static final class xml {
        public static final int preferences=0x7f040000;
    }
}
