package com.w12150.metadata.explorer;

import java.util.Calendar;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;

public class ClientList extends Activity {
	
	SQLiteDatabase db;
	ClientAdapter listAdapter;
	ListView clientList;
	Credential editCred;
	
	@Override
	public void onCreate(Bundle savedInstances) {
		super.onCreate(savedInstances);
		setContentView(R.layout.client_list);
		
		//Set up menubar
		ActionBar bar = getActionBar();
		bar.setTitle("Instance List");
		
		//Load database
		DataHelper dbHelper = new DataHelper(getApplicationContext());
		db = dbHelper.getWritableDatabase();
		editCred = null;
		
		
		//Load clients
		Credential[] credentialList = Credential.query(null, db);
		listAdapter = new ClientAdapter(this, R.layout.credential_list_item, credentialList);
		clientList = (ListView) findViewById(R.id.client_list_list);
		clientList.setAdapter(listAdapter);
		
		//Set load
		clientList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {
				Credential cred = listAdapter.getItem(pos);
				cred.lastUsedDate = Calendar.getInstance();
				listAdapter.notifyDataSetChanged();
				Intent loadOverview = new Intent(ClientList.this, Overview.class);
				loadOverview.putExtra("credId", String.valueOf(cred.getId()));
				ClientList.this.startActivity(loadOverview);
			}
			
		});
		
		//Set edit
		clientList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View v, int pos, long arg3) {
				editCred = listAdapter.getItem(pos);
				EditCredBox editBox = new EditCredBox(ClientList.this, editCred, pos);
				
				editBox.show();
				
				return false;
			}
			
		});
	}
	
	@Override
	public void onRestart() {
		super.onRestart();
		if(this.listAdapter != null) {
			this.listAdapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.client_list, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.add_credentail:
			AlertDialog.Builder addAlert = new AlertDialog.Builder(this);
			addAlert.setTitle("Add Client");
			LayoutInflater inf = getLayoutInflater();
			addAlert.setView(inf.inflate(R.layout.add_client_view, null));
			
			addAlert.setPositiveButton("Create", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					EditText uName = (EditText) ((AlertDialog) dialog).findViewById(R.id.new_client_uname);
					CheckBox sandboxBox = (CheckBox) ((AlertDialog) dialog).findViewById(R.id.new_client_is_sandbox);
					String username = uName.getText().toString();
					boolean useSandbox = false;
					if(sandboxBox.isChecked())
						useSandbox = true;
					if(username != null) {
						if(!username.equals("")) {
							Log.d("Database","Creating client " + username);
							Credential newCred = new Credential(username,null);
							newCred.isSand = useSandbox;
							newCred.insertUpdate(db);
							listAdapter = new ClientAdapter(ClientList.this, R.layout.credential_list_item, Credential.query(null, db));
							clientList.setAdapter(listAdapter);
						}
					}
				}
				
			});
			addAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
				}
				
			});
			
			addAlert.show();
			return true;
		case R.id.client_settings:
			Intent showSettings = new Intent(this, SettingsActivity.class);
			this.startActivity(showSettings);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	
	private class EditCredBox extends AlertDialog {
		
		Credential editCred;
		
		public EditCredBox(Context context, Credential editCred, int pos) {
			super(context);
			
			this.editCred = editCred;
			setTitle(editCred.username);
			this.setButton(this.BUTTON_NEGATIVE, "Close", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					
				}
				
			});
			
			LinearLayout holder = new LinearLayout(ClientList.this);
			holder.setOrientation(LinearLayout.VERTICAL);
			Button deleteButton = new Button(ClientList.this);
			deleteButton.setText("Delete Credential");
			deleteButton.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
			holder.addView(deleteButton, 0);
			Button editButton = new Button(ClientList.this);
			editButton.setText("Edit Credential");
			editButton.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
			holder.addView(editButton, 1);
			this.setView(holder);
			
			deleteButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Credential editCred = getEditCred();
					editCred.delete(db);
					listAdapter = new ClientAdapter(ClientList.this, R.layout.credential_list_item, Credential.query(null, db));
					clientList.setAdapter(listAdapter);
					EditCredBox.this.dismiss();
				}
				
			});
			
			editButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Credential editCred = EditCredBox.this.getEditCred();
					EditCredBox.this.dismiss();
					
					AlertDialog.Builder addAlert = new AlertDialog.Builder(ClientList.this);
					addAlert.setTitle("Edit Credential");
					LayoutInflater inf = getLayoutInflater();
					View holder = inf.inflate(R.layout.add_client_view, null);
					
					((EditText)holder.findViewById(R.id.new_client_uname)).setText(editCred.username);
					((CheckBox)holder.findViewById(R.id.new_client_is_sandbox)).setSelected(editCred.isSand);
					
					addAlert.setView(holder);
					
					addAlert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Credential editCred = EditCredBox.this.getEditCred();
							EditText uName = (EditText) ((AlertDialog) dialog).findViewById(R.id.new_client_uname);
							uName.setText(editCred.username);
							CheckBox sandboxBox = (CheckBox) ((AlertDialog) dialog).findViewById(R.id.new_client_is_sandbox);
							sandboxBox.setSelected(editCred.isSand);
							String username = uName.getText().toString();
							boolean useSandbox = false;
							if(sandboxBox.isChecked())
								useSandbox = true;
							if(username != null) {
								if(!username.equals("")) {
									editCred.isSand = useSandbox;
									editCred.username = uName.getText().toString();
									editCred.insertUpdate(db);
									listAdapter = new ClientAdapter(ClientList.this, R.layout.credential_list_item, Credential.query(null, db));
									clientList.setAdapter(listAdapter);
								}
							}
						}
						
					});
					addAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
						}
						
					});
					
					addAlert.show();
				}
				
			});
		}
		
		private Credential getEditCred() { return editCred; }
		
	}
	
	
	private class ClientAdapter extends ArrayAdapter<Credential> {
		
		Context context;
		int layoutRId;
		Credential[] data = null;
		
		public ClientAdapter(Context c, int tLayoutRId, Credential[] tData) {
			super(c, tLayoutRId, tData);
			this.context = c;
			this.layoutRId = tLayoutRId;
			this.data = tData;
		}
		
		@Override
		public View getView(int pos, View convertView, ViewGroup parent) {
			View row = convertView;
			viewHolder holder = null;
			
			if(row == null) {
				LayoutInflater inf = ((Activity)context).getLayoutInflater();
				row = inf.inflate(R.layout.credential_list_item, parent, false);
				holder = new viewHolder();
				holder.nameText = (TextView) row.findViewById(R.id.cred_item_name);
				holder.detailText = (TextView) row.findViewById(R.id.cred_item_details);
				
				row.setTag(holder);
			} else {
				holder = (viewHolder) row.getTag();
			}
			
			Credential cred = data[pos];
			
			holder.nameText.setText(cred.username);
			Calendar viewDate = cred.createdDate;
			if(cred.lastUsedDate != null) {
				String prettyDate = DataHelper.prettyDate(cred.lastUsedDate);
				holder.detailText.setText(prettyDate);
			} else {
				holder.detailText.setText("Not Authenticated");
			}
			
			return row;
		}
		
		private class viewHolder {
			TextView nameText;
			TextView detailText;
		}
		
	}
	
}
