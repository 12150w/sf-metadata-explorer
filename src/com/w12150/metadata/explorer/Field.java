package com.w12150.metadata.explorer;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Field {
	
	public String name, type, label, referenceTo, relationshipName;
	public String defaultText;
	public boolean isCustom, isRequired, isUpdatable;
	public ArrayList<String> pickVals;
	public int length, digits, precision;
	public int inputId;
	
	public Field(String name) {
		this.name = name;
		isCustom = false;
		isRequired = false;
		isUpdatable = true;
		defaultText = "";
	}
	
	public Field(JSONObject jsonObj) {
		try {
			this.name = jsonObj.getString("name");
			this.label = jsonObj.getString("label");
			this.type = jsonObj.getString("type");
			this.isRequired = !jsonObj.getBoolean("nillable");
			this.isUpdatable = jsonObj.getBoolean("updateable");
			this.isCustom = jsonObj.getBoolean("custom");
			
			this.defaultText = "API Name: " + this.name + "\n\n";
			
			if(this.type.equals("picklist")) {
				//PICKLIST
				JSONArray pickOps = jsonObj.getJSONArray("picklistValues");
				ArrayList<String> pickVals = new ArrayList<String>();
				for(int j=0;j<pickOps.length();j++) {
					try {
						JSONObject pickOption = pickOps.getJSONObject(j);
						String value = pickOption.getString("value");
						JSONArray temp = pickOption.names();
						if(pickOption.getBoolean("defaultValue")) {
							value += "*";
						}
						pickVals.add(value);
					} catch (JSONException e) {
						break;
					}
				}
				this.pickVals = pickVals;
			} else if(this.type.equals("reference")) {
				//REFERENCE
				JSONArray referenceTo = jsonObj.getJSONArray("referenceTo");
				String rName = jsonObj.getString("relationshipName");
				String referenceToText = "Reference To:";
				for(int j=0;j<referenceTo.length();j++)
					referenceToText += " " + referenceTo.getString(j) + ",";
				referenceToText = DataHelper.trimEnd(referenceToText, ",");
				this.referenceTo = referenceToText;
				this.relationshipName = rName;
			} else if(this.type.equals("string")) {
				//TEXT
				this.length = jsonObj.getInt("length");
			} else if(this.type.equals("double")) {
				//DOUBLE
				JSONArray options = jsonObj.names();
				for(int i=0;i<options.length();i++) {
					Log.e("SalesForce", options.getString(i));
				}
				int scale = jsonObj.getInt("scale");
				int precision = jsonObj.getInt("precision");
				this.digits = precision - scale;
				this.precision = scale;
			} else if(this.type.equals("textarea")) {
				this.length = jsonObj.getInt("length");
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public String pack() {
		return this.name + "|" + this.type + ";";
	}
	
	public static Field[] parsePack(String pack) {
		String[] rawFields = pack.split(";");
		Field[] fieldList = new Field[rawFields.length];
		int i=0;
		Log.d("Test", "Raw field parse: " + pack);
		for(String rawField : rawFields) {
			Log.d("Test", "Parsing field raw: " + rawField);
			String[] parts = rawField.split("\\|");
			Field field = new Field(parts[0]);
			field.type = parts[1];
			fieldList[i] = field;
			i++;
			Log.d("Test","Parsed field {" + field.name + " + " + field.type);
		}
		return fieldList;
	}
	
	public View getInputView(Context context) {
		if(type.equals("string")) {
			//STRING
			Log.d("Test","Creating input for field");
			EditText input = new EditText(context);
			InputFilter maxLength = new InputFilter.LengthFilter(this.length);
			input.setFilters(new InputFilter[] { maxLength });
			input.setHint(this.name);
			input.setId(this.name.hashCode());
			return input;
		} if(type.equals("double")) {
			//Double
			EditText numIn = new EditText(context);
			numIn.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
			numIn.setHint(this.name);
			numIn.setId(this.name.hashCode());
			return numIn;
		} else if(type.equals("reference")) {
			//LOOKUP
			View holder = ((Activity) context).getLayoutInflater().inflate(R.layout.lookup_line_layout, null);
			Button lButton = (Button) holder.findViewById(R.id.line_lookup_button);
			TextView lLabel = (TextView) holder.findViewById(R.id.line_lookup_text);
			lLabel.setText(this.name);
			return holder;
		}
		return null;
	}
}
