package com.w12150.metadata.explorer;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class ObjectDetails extends Activity{
	
	GlobalState state;
	ListView fieldList;
	FieldAdapter fieldAdapter;
	String objectName;
	Credential thisCred;
	SQLiteDatabase db;
	sObject thisObj;
	
	@Override
	public void onCreate(Bundle savedInstances) {
		super.onCreate(savedInstances);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.object_detail);
		
		//Load state
		state = (GlobalState) getApplication();
		Intent sourceIntent = getIntent();
		Bundle sourceExtras = sourceIntent.getExtras();
		objectName = sourceExtras.getString("sObject_Name");
		thisObj = new sObject(sourceExtras.getString("sObject_Name"));
		String credId = sourceExtras.getString("credId");
		
		//Load style
		ActionBar actionBar = getActionBar();
		actionBar.setTitle(objectName);
		setProgressBarIndeterminateVisibility(true);
		
		//Load Database
		DataHelper dbHelper = new DataHelper(getApplicationContext());
		db = dbHelper.getReadableDatabase();
		
		//Load Credential
		thisCred = Credential.query("WHERE " + DataHelper.ID_KEY + " = " + credId, db)[0];
		
		//Get views
		fieldList = (ListView) findViewById(R.id.object_detail_list);
		fieldList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {
				Field selectedField = fieldAdapter.getItem(pos);
				showFieldInfo(selectedField);
			}
			
		});
		
		//Load data
		String describeUrl = "/services/data/v26.0/sobjects/" + objectName + "/describe";
		LoadFieldTask loadTask = new LoadFieldTask(this);
		loadTask.execute(thisCred.endUrl + describeUrl + "/");
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.object_detail, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.object_detail_create:
			Intent createObject = new Intent(this, CreateObject.class);
			
			String fieldPack = "";
			for(int i=0;i<fieldAdapter.getCount();i++) {
				fieldPack += fieldAdapter.getItem(i).pack();
			}
			if(fieldPack.endsWith(";"))
				fieldPack = DataHelper.trimEnd(fieldPack, ";");
			createObject.putExtra("fields", fieldPack);
			
			this.startActivity(createObject);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	
	
	
	private class LoadFieldTask extends AsyncTask<String, Field, Field[]> {
		
		Context context;
		
		public LoadFieldTask(Context c) {
			context = c;
		}
		
		@Override
		protected Field[] doInBackground(String... url) {
			HttpGet getRequest = new HttpGet(url[0]);
			getRequest.addHeader("Authorization","OAuth " + thisCred.apiToken);
			
			DefaultHttpClient client = new DefaultHttpClient();
			try {
				HttpResponse response = client.execute(getRequest);
				String result = EntityUtils.toString(response.getEntity());
				Log.d("SalesForce","Loaded:\n" + result);
				JSONObject object = (JSONObject) new JSONTokener(result).nextValue();
				
				if(object.has("errorCode")) {
					if(object.getString("errorCode").equals("INVALID_SESSION_ID")) {
						Log.e("SalesForce","REFRESHING API TOKEN");
						thisCred.apiToken = SForceUtil.refresh(state.CLIENT_ID, state.CLIENT_SECRET, thisCred.refreshToken, thisCred, db);
						response = client.execute(getRequest);
						result = EntityUtils.toString(response.getEntity());
						Log.d("SalesForce",result);
						object = (JSONObject) new JSONTokener(result).nextValue();
					}
				}
				
				JSONArray jsonList = object.getJSONArray("fields");
				Field[] fieldList = new Field[jsonList.length()];
				for(int i=0;i<jsonList.length();i++) {
					JSONObject jsonObj = (JSONObject) jsonList.get(i);
					Field field = new Field(jsonObj);
					
					fieldList[i] = field;
					publishProgress(field);
				}
				
				//Sort list
				Field[] sortedList = new Field[fieldList.length];
				int countUp = 0;
				for(Field f : fieldList) {
					if(f.isCustom) {
						sortedList[countUp] = f;
						countUp ++;
					}
				}
				for(Field f : fieldList) {
					if(!f.isCustom) {
						sortedList[countUp] = f;
						countUp++;
					}
				}
				return sortedList;
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		protected void onProgressUpdate(sObject... field) {
			
		}
		
		@Override
		protected void onPostExecute(Field[] dataList) {
			if(dataList != null) {
				fieldAdapter = new FieldAdapter(context, R.layout.overview_list_item, dataList);
				fieldList.setAdapter(fieldAdapter);
			}
			ObjectDetails.this.setProgressBarIndeterminateVisibility(false);
		}
		
	}
	
	
	private class FieldAdapter extends ArrayAdapter<Field> {
		
		Context context;
		int layoutRId;
		Field[] data = null;
		
		public FieldAdapter(Context c, int tLayoutRId, Field[] tData) {
			super(c, tLayoutRId, tData);
			this.context = c;
			this.layoutRId = tLayoutRId;
			this.data = tData;
		}
		
		@Override
		public View getView(int pos, View convertView, ViewGroup parent) {
			View row = convertView;
			viewHolder holder = null;
			
			if(row == null) {
				LayoutInflater inf = ((Activity)context).getLayoutInflater();
				row = inf.inflate(R.layout.overview_list_item, parent, false);
				holder = new viewHolder();
				holder.nameText = (TextView) row.findViewById(R.id.overview_list_item_name);
				holder.labelText = (TextView) row.findViewById(R.id.overview_list_item_label);
				holder.typeText = (TextView) row.findViewById(R.id.overview_list_item_prefix);
				
				row.setTag(holder);
			} else {
				holder = (viewHolder) row.getTag();
			}
			
			Field rObject = data[pos];
			holder.nameText.setText(rObject.name);
			holder.labelText.setText(rObject.label);
			
			String objPref = rObject.type;
			if(!objPref.equals("null"))
				holder.typeText.setText(" {" + rObject.type + "} ");
			
			if(rObject.isRequired && rObject.isUpdatable) {
				for(TextView v : holder.getTextViews()) {
					v.setTextColor(Color.parseColor("#9932CC"));
				}
			} else {
				for(TextView v : holder.getTextViews()) {
					v.setTextColor(Color.BLACK);
				}
			}
			
			return row;
		}
		
		
		
		private class viewHolder {
			TextView nameText;
			TextView labelText;
			TextView typeText;
			
			public TextView[] getTextViews() {
				TextView[] views = new TextView[]{nameText,labelText,typeText};
				return views;
			}
		}
		
	}
	
	private void showFieldInfo(Field field) {
		AlertDialog.Builder detailAlert = new AlertDialog.Builder(this);
		detailAlert.setTitle(field.label);
		LayoutInflater inf = getLayoutInflater();
		View detailView = inf.inflate(R.layout.field_details, null);
		TextView text = (TextView) detailView.findViewById(R.id.field_details_text);
		text.setText(field.defaultText);
		
		String message = "";
		if(field.type.equals("picklist")) {
			//PICKLIST
			message = "Picklist Options:\n";
			for(String s : field.pickVals)
				message += "  " + s + "\n";
		} else if(field.type.equals("reference")) {
			//REFERENCE
			message += "Relationship Name: " + field.relationshipName + "\n\n";
			message += field.referenceTo;
		} else if(field.type.equals("string") || field.type.equals("textarea")) {
			//TEXT & TEXTAREA
			message += "Length: " + field.length;
			
		} else if(field.type.equals("double")) {
			//DOUBLE
			message += "Digits: " + field.digits + "\n\n";
			message += "Decimal Points: " + field.precision;
			
		}
		text.setText(text.getText().toString() + message);
		
		detailAlert.setView(detailView);
		
		detailAlert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
			
		});
		
		detailAlert.show();
	}
	
}
