package com.w12150.metadata.explorer;

import java.util.Calendar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataHelper extends SQLiteOpenHelper {
	
	private static final int DB_VERSION = 8;
	private static final String DB_NAME = "Database.db";
	
	public static final String ID_KEY = "_id";
	public static final String TMP_KEY = "_tmp";
	public static final String TRUE = "yes";
	public static final String FALSE = "no";
	
	public static final String CRD_TB_NAME = "Credential";
	public static final String CRD_USERNAME = "Username";
	public static final String CRD_PASSWORD = "Password";
	public static final String CRD_LAST_USED = "Last_Used_Time";
	public static final String CRD_CREATE = "Created_Time";
	public static final String CRD_TOKEN = "Security_Token";
	public static final String CRD_API_TOKEN = "Access_Token";
	public static final String CRD_REFRESH_TOKEN = "Refresh_Token";
	public static final String CRD_GO_BAD = "Go_Bad_Time";
	public static final String CRD_END_URL = "Endpoint_Url";
	public static final String CRD_IS_SAND = "Is_Sandbox";
	public static final String CRD_ALL = CRD_USERNAME + ", " +
			CRD_PASSWORD + ", " +
			CRD_LAST_USED + ", " +
			CRD_CREATE + ", " +
			CRD_TOKEN + ", " +
			CRD_API_TOKEN + ", " +
			CRD_REFRESH_TOKEN + ", " +
			CRD_GO_BAD + ", " +
			CRD_END_URL + ", " +
			CRD_IS_SAND + ", " +
			ID_KEY + ", " +
			TMP_KEY;
	private static final String CRD_CREATE_SQL = "CREATE TABLE " + CRD_TB_NAME + " (" +
			CRD_USERNAME + " TEXT, " +
			CRD_PASSWORD + " TEXT, " +
			CRD_LAST_USED + " TEXT, " +
			CRD_CREATE + " TEXT, " +
			CRD_TOKEN + " TEXT, " +
			CRD_API_TOKEN + " TEXT, " +
			CRD_REFRESH_TOKEN + " TEXT, " +
			CRD_GO_BAD + " TEXT, " +
			CRD_END_URL + " TEXT, " +
			CRD_IS_SAND + " TEXT, " +
			TMP_KEY + " TEXT, " +
			ID_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT" +
			")";
	
	public DataHelper(Context c) {
		super(c, DB_NAME, null, DB_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d("DataHelper","Creating Table: " + CRD_TB_NAME);
		db.execSQL(CRD_CREATE_SQL);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
		Log.d("DataHelper","Wiping Table: " + CRD_TB_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + CRD_TB_NAME);
		db.execSQL(CRD_CREATE_SQL);
	}
	
	
	public static Calendar parseDate(String rawDate) {
		if(rawDate != null) {
			String[] parts = rawDate.split("-");
			Log.d("Database","PARSING: " + rawDate + " {" + parts.length + "}");
			Calendar date = Calendar.getInstance();
			for(int i=0;i<parts.length;i++) {
				switch(i) {
				case 0:
					date.set(Calendar.YEAR, Integer.valueOf(parts[i]));
					break;
				case 1:
					date.set(Calendar.MONTH, Integer.valueOf(parts[i]));
					break;
				case 2:
					date.set(Calendar.DAY_OF_MONTH, Integer.valueOf(parts[i]));
					break;
				case 3:
					date.set(Calendar.HOUR_OF_DAY, Integer.valueOf(parts[i]));
					break;
				case 4:
					date.set(Calendar.MINUTE, Integer.valueOf(parts[i]));
					break;
				case 5:
					date.set(Calendar.SECOND, Integer.valueOf(parts[i]));
					break;
				}
			}
			return date;
		} else {
			return null;
		}
	}
	
	public static String stringDate(Calendar date) {
		if(date != null) {
			String strungDate = "";
			strungDate += date.get(Calendar.YEAR) + "-";
			strungDate += date.get(Calendar.MONTH) + "-";
			strungDate += date.get(Calendar.DAY_OF_MONTH) + "-";
			strungDate += date.get(Calendar.HOUR_OF_DAY) + "-";
			strungDate += date.get(Calendar.MINUTE) + "-";
			strungDate += date.get(Calendar.SECOND);
			return strungDate;
		} else {
			return null;
		}
	}
	
	public static String wordMonth(int month) {
		String monthName;
		switch(month) {
		case 0:
			monthName = "January";
			break;
		case 1:
			monthName = "February";
			break;
		case 2:
			monthName = "March";
			break;
		case 3:
			monthName = "April";
			break;
		case 4:
			monthName = "May";
			break;
		case 5:
			monthName = "June";
			break;
		case 6:
			monthName = "July";
			break;
		case 7:
			monthName = "August";
			break;
		case 8:
			monthName = "September";
			break;
		case 9:
			monthName = "October";
			break;
		case 10:
			monthName = "November";
			break;
		case 11:
			monthName = "December";
			break;
		default:
			monthName = null;
		}
		return monthName;
	}
	
	public static String prettyDate(Calendar viewDate) {
		if(viewDate != null) {
			String month = DataHelper.wordMonth(viewDate.get(Calendar.MONTH));
			String day = String.valueOf(viewDate.get(Calendar.DAY_OF_MONTH));
			String year = String.valueOf(viewDate.get(Calendar.YEAR));
			String hour = String.valueOf(viewDate.get(Calendar.HOUR));
			String minute = String.valueOf(viewDate.get(Calendar.MINUTE));
			String second = String.valueOf(viewDate.get(Calendar.SECOND));
			
			if(day.length() == 1) day = "0" + day;
			if(hour.length() == 1) hour = "0" + hour;
			if(minute.length() == 1) minute = "0" + minute;
			if(second.length() == 1) second = "0" + second;
			
			String prettyDate = month + " " + day + ", " + year + " "
					+ hour + ":" + minute + ":" + second + " ";
			
			if(viewDate.get(Calendar.AM_PM) == Calendar.AM)
				prettyDate += "AM";
			else
				prettyDate += "PM";
			
			return prettyDate;
		} else {
			return null;
		}
	}
	
	public static String trimEnd(String base, String end) {
		if(base != null) {
			if(base.endsWith(end)) {
				base = base.substring(0, base.length() - end.length());
			}
		}
		return base;
	}
	
}
