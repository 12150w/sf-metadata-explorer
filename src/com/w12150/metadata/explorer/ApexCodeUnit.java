package com.w12150.metadata.explorer;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;


public class ApexCodeUnit {
	
	public String body, name, id;
	public Boolean isActive;
	public String type;
	
	
	
	public static ApexCodeUnit[] getCode(String scope, Credential cred, SQLiteDatabase db, GlobalState state, Context c) {
		LoadCodeTask loadTask = new LoadCodeTask(c, cred, db, state);
		return loadTask.doInBackground(scope);
	}
	
	
	private static class LoadCodeTask extends AsyncTask<String, Void, ApexCodeUnit[]> {
		
		private Context context;
		private Credential thisCred;
		private SQLiteDatabase db;
		private GlobalState state;
		
		public LoadCodeTask(Context c, Credential cred, SQLiteDatabase db, GlobalState state) {
			this.context = c;
			thisCred = cred;
			this.db = db;
			this.state = state;
		}

		@Override
		protected ApexCodeUnit[] doInBackground(String... params) {
			String scope = params[0];
			
			//Make query
			String apexQuery = "SELECT Id, Name, Status, Body FROM ApexClass";
			HttpGet getRequest = new HttpGet(thisCred.endUrl + "/services/data/v26.0/query/?q="+apexQuery.replaceAll(" ", "%20"));
			
			//Get token
			DefaultHttpClient client = new DefaultHttpClient();
			
			getRequest.addHeader("Authorization","OAuth " + thisCred.apiToken);
			
			try {
				
				HttpResponse response = client.execute(getRequest);
				String result = EntityUtils.toString(response.getEntity());
				Log.d("SalesForce",result);
				JSONObject object = null;
				try {
					object = (JSONObject) new JSONTokener(result).nextValue();
				} catch (ClassCastException e) {
					if(result.toString().contains("INVALID_SESSION_ID")) {
						Log.e("SalesForce","REFRESHING API TOKEN");
						thisCred.apiToken = SForceUtil.refresh(state.CLIENT_ID, state.CLIENT_SECRET, thisCred.refreshToken, thisCred, db);
						thisCred.insertUpdate(db);
						Log.d("SalesForce", "Setting new auth header");
						getRequest.removeHeaders("Authorization");
						getRequest.addHeader("Authorization", "OAuth " + thisCred.apiToken);
						response = client.execute(getRequest);
						result = EntityUtils.toString(response.getEntity());
						Log.d("SalesForce",result);
						object = (JSONObject) new JSONTokener(result).nextValue();
					}
				}
				
				JSONArray jsonList = object.getJSONArray("records");
				ApexCodeUnit[] codeList = new ApexCodeUnit[jsonList.length()];
				for(int i=0;i<jsonList.length();i++) {
					JSONObject jsonObj = (JSONObject) jsonList.get(i);
					//TODO: take the json object and create an ApexCodeUnit Object
				}
				return codeList;
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}
		
	}
	
}
