package com.w12150.metadata.explorer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Calendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Overview extends Activity {
	
	WebView loginView;
	GlobalState state;
	RelativeLayout base;
	ListView sObjectList;
	sObjectAdapter dataAdapter;
	ActionBar theBar;
	String apiToken, refreshToken, endUrl;
	Calendar goBad;
	SQLiteDatabase db;
	Credential thisCred;
	boolean refreshed;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.overview);
		
		//Set up action bar
		theBar = getActionBar();
		setProgressBarIndeterminateVisibility(true);
		theBar.setDisplayHomeAsUpEnabled(true);
		
		
		//Lookup Views
		loginView = (WebView) findViewById(R.id.overview_login);
		base = (RelativeLayout) findViewById(R.id.overview_base);
		sObjectList = new ListView(this);
		
		//Load Database
		DataHelper dbHelper = new DataHelper(getApplicationContext());
		db = dbHelper.getReadableDatabase();
		
		//Load extras
		Bundle passedStuff = getIntent().getExtras();
		String credId = passedStuff.getString("credId");
		thisCred = Credential.query("WHERE " + DataHelper.ID_KEY + " = " + credId, db)[0];
		refreshed = false;
		
		//Load Token
		state = (GlobalState) getApplication();
		if(thisCred.refreshToken == null) {
			//Setup for login
			theBar.setTitle("Please Login");
			
			if(thisCred.isSand)
				Log.e("SalesForce","USE SANDBOX");
			
			//Get Credentials
			WebSettings loginSettings = loginView.getSettings();
			loginSettings.setJavaScriptEnabled(true);
			
			loginView.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView v, String url) {
					Log.d("TMP","Return url: " + url);
					
					if(url.startsWith("timetracker://callback")) {
						GlobalState state = (GlobalState) getApplication();
						parseOAuthResponse(url,thisCred);
						Intent authIntent = new Intent(Overview.this,Overview.class);
						authIntent.putExtra("credId", String.valueOf(thisCred.getId()));
						Overview.this.startActivity(authIntent);
						return true;
					} else {
						return false;
					}
				}
				@Override
				public void onPageFinished(WebView view, String url) {
					Overview.this.setProgressBarIndeterminateVisibility(false);
				}
				@Override
				public void onPageStarted(WebView view, String url, Bitmap favicon) {
					Overview.this.setProgressBarIndeterminateVisibility(true);
				}
			});
			if(thisCred.isSand)
				loginView.loadUrl(state.SAND_AUTH_URL);
			else
				loginView.loadUrl(state.AUTH_URL);
		} else {
			//Load all sObjects
			theBar.setTitle("All sObjects");
			loginView.setVisibility(View.GONE);
			base.addView(sObjectList);
			LoadObjectTask loadObjects = new LoadObjectTask(this);
			loadObjects.execute(thisCred.endUrl + "/services/data/v26.0/sobjects");
			
			sObjectList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {
					sObject selectedItem = dataAdapter.getItem(pos);
					String objectName = selectedItem.name;
					
					Intent objectDetails = new Intent(Overview.this, ObjectDetails.class);
					objectDetails.putExtra("sObject_Name", objectName);
					objectDetails.putExtra("credId", String.valueOf(thisCred.getId()));
					Overview.this.startActivity(objectDetails);
				}
				
			});
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.overview, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.menu_logout:
			//This is actually the Apex Code option
			Intent showCode = new Intent(this, ApexCodeView.class);
			showCode.putExtra("w12150_scope", "all");
			this.startActivity(showCode);
			return true;
		case R.id.menu_settings:
			Intent showSettings = new Intent(this, SettingsActivity.class);
			this.startActivity(showSettings);
			return true;
		case android.R.id.home:
			Intent showInstances = new Intent(this, ClientList.class);
			this.startActivity(showInstances);
			return true;
		case R.id.menu_query:
			Intent startSOQL = new Intent(this, SOQLView.class);
			this.startActivity(startSOQL);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	
	private void parseOAuthResponse(String url, Credential cred) {
		String temp = url.split("#")[1];
		String[] keypairs = temp.split("&");
		for (int i=0;i<keypairs.length;i++) {
			String[] onepair = keypairs[i].split("=");
			if (onepair[0].equals("access_token")) {
				try {
					cred.apiToken = URLDecoder.decode(onepair[1],"UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			} else if (onepair[0].equals("refresh_token")) {
				cred.refreshToken = onepair[1];
			} else if (onepair[0].equals("instance_url")) {
				String instanceUrl = onepair[1].replaceAll("%2F", "/").replaceAll("%3A", ":");
				cred.endUrl = instanceUrl;
			} else if (onepair[0].equals("id")) {
				//myTokens.set_id(onepair[1]);
			} else if (onepair[0].equals("issued_at")) {
				//myTokens.set_issued_at(Long.valueOf(onepair[1]));
			} else if (onepair[0].equals("signature")) {
				//myTokens.set_signature(onepair[1]);
			}
		}
		cred.insertUpdate(db);
	}
	
	private class LoadObjectTask extends AsyncTask<String, sObject, sObject[]> {
		
		Context context;
		
		public LoadObjectTask(Context c) {
			context = c;
		}
		
		@Override
		protected sObject[] doInBackground(String... url) {
			HttpGet getRequest = new HttpGet(url[0]);
			//Get token
			DefaultHttpClient client = new DefaultHttpClient();
			
			getRequest.addHeader("Authorization","OAuth " + thisCred.apiToken);
			
			try {
				thisCred.lastUsedDate = Calendar.getInstance();
				thisCred.insertUpdate(db);
				
				HttpResponse response = client.execute(getRequest);
				String result = EntityUtils.toString(response.getEntity());
				Log.d("SalesForce",result);
				JSONObject object = null;
				try {
					object = (JSONObject) new JSONTokener(result).nextValue();
				} catch (ClassCastException e) {
					if(result.toString().contains("INVALID_SESSION_ID")) {
						Log.e("SalesForce","REFRESHING API TOKEN");
						thisCred.apiToken = SForceUtil.refresh(state.CLIENT_ID, state.CLIENT_SECRET, thisCred.refreshToken, thisCred, db);
						thisCred.insertUpdate(db);
						Log.d("SalesForce", "Setting new auth header");
						getRequest.removeHeaders("Authorization");
						getRequest.addHeader("Authorization", "OAuth " + thisCred.apiToken);
						response = client.execute(getRequest);
						result = EntityUtils.toString(response.getEntity());
						Log.d("SalesForce",result);
						object = (JSONObject) new JSONTokener(result).nextValue();
					}
				}
				
				JSONArray jsonList = object.getJSONArray("sobjects");
				sObject[] sObjectList = new sObject[jsonList.length()];
				for(int i=0;i<jsonList.length();i++) {
					JSONObject jsonObj = (JSONObject) jsonList.get(i);
					sObject sObject = new sObject(jsonObj.getString("name"));
					sObject.label = jsonObj.getString("label");
					sObject.prefix = jsonObj.getString("keyPrefix");
					if(jsonObj.getBoolean("custom"))
						sObject.isCustom = true;
					sObjectList[i] = sObject;
					publishProgress(sObject);
				}
				//Sort list
				sObject[] sortedList = new sObject[sObjectList.length];
				int countUp = 0;
				for(sObject o : sObjectList) {
					if(o.isCustom) {
						sortedList[countUp] = o;
						countUp ++;
					}
				}
				for(sObject o : sObjectList) {
					if(!o.isCustom) {
						sortedList[countUp] = o;
						countUp++;
					}
				}
				return sortedList;
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		protected void onProgressUpdate(sObject... sObj) {
			
		}
		
		@Override
		protected void onPostExecute(sObject[] dataList) {
			if(dataList != null) {
				dataAdapter = new sObjectAdapter(context, R.layout.overview_list_item, dataList);
				sObjectList.setAdapter(dataAdapter);
			}
			Overview.this.setProgressBarIndeterminateVisibility(false);
		}
		
	}
	
	
	private class sObjectAdapter extends ArrayAdapter<sObject> {
		
		Context context;
		int layoutRId;
		sObject[] data = null;
		
		public sObjectAdapter(Context c, int tLayoutRId, sObject[] tData) {
			super(c, tLayoutRId, tData);
			this.context = c;
			this.layoutRId = tLayoutRId;
			this.data = tData;
		}
		
		@Override
		public View getView(int pos, View convertView, ViewGroup parent) {
			View row = convertView;
			viewHolder holder = null;
			
			if(row == null) {
				LayoutInflater inf = ((Activity)context).getLayoutInflater();
				row = inf.inflate(R.layout.overview_list_item, parent, false);
				holder = new viewHolder();
				holder.nameText = (TextView) row.findViewById(R.id.overview_list_item_name);
				holder.labelText = (TextView) row.findViewById(R.id.overview_list_item_label);
				holder.prefText = (TextView) row.findViewById(R.id.overview_list_item_prefix);
				
				row.setTag(holder);
			} else {
				holder = (viewHolder) row.getTag();
			}
			
			sObject rObject = data[pos];
			holder.nameText.setText(rObject.name);
			holder.labelText.setText(rObject.label);
			
			String objPref = rObject.prefix;
			if(!objPref.equals("null"))
				holder.prefText.setText(" {" + rObject.prefix + "} ");
			
			return row;
		}
		
		
		
		private class viewHolder {
			TextView nameText;
			TextView labelText;
			TextView prefText;
		}
		
	}
	
}
