package com.w12150.metadata.explorer;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class CreateObject extends Activity {
	
	Field[] fieldList;
	LinearLayout holder;
	Button saveButton, cancelButton;
	
	@Override
	public void onCreate(Bundle savedInstances) {
		super.onCreate(savedInstances);
		setContentView(R.layout.create_object);
		
		//Get views
		holder = (LinearLayout) findViewById(R.id.create_object_holder);
		cancelButton = (Button) findViewById(R.id.create_object_cancel);
		saveButton = (Button) findViewById(R.id.create_object_create);
		
		//Button actions
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				CreateObject.this.finish();
			}
			
		});
		
		//Parse field info
		String fieldBundle = getIntent().getStringExtra("fields");
		fieldList = Field.parsePack(fieldBundle);
		
		//Show fields
		for(Field field : fieldList) {
			View fieldInput = field.getInputView(this);
			if(fieldInput != null) {
				Log.d("Test","Adding view for " + field.name);
				fieldInput.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				holder.addView(fieldInput, 1);
			}
		}
	}
	
}
