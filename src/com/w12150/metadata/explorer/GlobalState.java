package com.w12150.metadata.explorer;

import android.app.Application;

public class GlobalState extends Application {
	
	private static final String RESPONSE_TYPE = "token";
	private static final String REDIRECT_URL = "timetracker%3A%2F%2Fcallback";
	public static final String CLIENT_ID = "3MVG9y6x0357Hlednau1pM4mPMa5_buENaCgTcVh9jcqMICb_hvieR0SnDw3HnUvTitbwfvSot401ggwyIdcW";
	
	public static final String AUTH_URL = "https://login.salesforce.com/services/oauth2/authorize" +
			"?response_type=" + RESPONSE_TYPE +
			"&redirect_uri=" + REDIRECT_URL +
			"&client_id=" + CLIENT_ID;
	public static final String SAND_AUTH_URL = "https://test.salesforce.com/services/oauth2/authorize" +
			"?response_type=" + RESPONSE_TYPE +
			"&redirect_uri=" + REDIRECT_URL +
			"&client_id=" + CLIENT_ID;
	public static final String CLIENT_SECRET = "2258919773686216003";
	
	
	private String clientId;
	private String accessToken;
	private String refreshToken;
	private String instanceUrl;
	
	
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getInstanceUrl() {
		return instanceUrl;
	}
	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}
	
}