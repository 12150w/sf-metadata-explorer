package com.w12150.metadata.explorer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class SForceUtil {
	
	public static String refresh(String clientId, String clientSecret, String refreshToken, Credential thisCred, SQLiteDatabase db) {
		DefaultHttpClient client = new DefaultHttpClient();
		String parameters = "grant_type=refresh_token&client_id=" + clientId +
				"&client_secret=" + clientSecret + "&refresh_token=" + thisCred.refreshToken;
		String baseUrl;
		if(thisCred.isSand)
			baseUrl = "https://test.salesforce.com";
		else
			baseUrl = "https://login.salesforce.com";
		HttpPost refreshPost = new HttpPost(baseUrl + "/services/oauth2/token");
		
		try {
			StringEntity rawBody = new StringEntity(parameters);
			rawBody.setContentType("application/x-www-form-urlencoded");
			refreshPost.setEntity(rawBody);
			Log.d("SalesForce","ENDPOINT: " + baseUrl + "/services/oauth2/token");
			Log.d("SalesForce","POSTING AUTH:\n" + parameters);
			HttpResponse authResponse = client.execute(refreshPost);
			String authInfo = EntityUtils.toString(authResponse.getEntity());
			Log.d("SalesForce","AUTH RESPONSE:\n" + authInfo);
			JSONObject response = (JSONObject) new JSONTokener(authInfo).nextValue();
			thisCred.apiToken = response.getString("access_token");
			//thisCred.insertUpdate(db);
			return thisCred.apiToken;
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
}
