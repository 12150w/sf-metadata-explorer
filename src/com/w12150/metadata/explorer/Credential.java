package com.w12150.metadata.explorer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Credential {
	
	public String username, password, securityToken, accessToken, apiToken, refreshToken, endUrl;
	public Calendar lastUsedDate, createdDate, goBadTime;
	public boolean isSand;
	private String id,tmpId;
	
	public Credential(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public void insertUpdate(SQLiteDatabase db) {
		if(id == null) {
			//Insert Object
			String insertSQL = "INSERT INTO " + DataHelper.CRD_TB_NAME + "(";
			this.createdDate = Calendar.getInstance();
			this.tmpId = String.valueOf(Math.random());
			
			
			if(username != null) insertSQL += DataHelper.CRD_USERNAME + ", ";
			if(password != null) insertSQL += DataHelper.CRD_PASSWORD + ", ";
			if(securityToken != null) insertSQL += DataHelper.CRD_TOKEN + ", ";
			if(createdDate != null) insertSQL += DataHelper.CRD_CREATE + ", ";
			if(goBadTime != null) insertSQL += DataHelper.CRD_GO_BAD + ", ";
			if(apiToken != null) insertSQL += DataHelper.CRD_API_TOKEN + ", ";
			if(refreshToken != null) insertSQL += DataHelper.CRD_REFRESH_TOKEN + ", ";
			if(endUrl != null) insertSQL += DataHelper.CRD_END_URL + ", ";
			insertSQL += DataHelper.CRD_IS_SAND + ", ";
			insertSQL += DataHelper.TMP_KEY;
			
			insertSQL += ") VALUES (";
			if(username != null) insertSQL += "'" + this.username + "', ";
			if(password != null) insertSQL += "'" + this.password + "', ";
			if(securityToken != null) insertSQL += "'" + this.securityToken + "', ";
			if(createdDate != null) insertSQL += "'" + DataHelper.stringDate(this.createdDate) + "', ";
			if(goBadTime != null) insertSQL += "'" + DataHelper.stringDate(this.goBadTime) + "', ";
			if(apiToken != null) insertSQL += "'" + this.apiToken + "', ";
			if(refreshToken != null) insertSQL += "'" + this.refreshToken + "', ";
			if(endUrl != null) insertSQL += "'" + this.endUrl + "', ";
			if(isSand) insertSQL += "'" + DataHelper.TRUE + "', ";
			else insertSQL += "'" + DataHelper.FALSE + "', ";
			insertSQL += "'" + this.tmpId + "')";
			
			Log.d("Database","EXECUTE INSERT:\n" + insertSQL);
			db.execSQL(insertSQL);
			
			//Get object id
			String selectSQL = "SELECT " + DataHelper.ID_KEY + " FROM " + DataHelper.CRD_TB_NAME +
					" WHERE " + DataHelper.TMP_KEY + " = '" + this.tmpId + "' " +
					"AND " + DataHelper.CRD_CREATE + " = '" + DataHelper.stringDate(this.createdDate) + "'";
			Cursor c = db.rawQuery(selectSQL, null);
			c.moveToFirst();
			this.id = c.getString(c.getColumnIndex(DataHelper.ID_KEY));
			Log.d("Database","NEW ID: " + this.id);
		} else {
			//update Object
			String sqlUpdate = "UPDATE " + DataHelper.CRD_TB_NAME + " SET ";
			if(username != null) sqlUpdate += DataHelper.CRD_USERNAME + " = '" + this.username + "', ";
			if(password != null) sqlUpdate += DataHelper.CRD_PASSWORD + " = '" + this.password + "', ";
			if(securityToken != null) sqlUpdate += DataHelper.CRD_TOKEN + " = '" + this.securityToken + "', ";
			if(createdDate != null) sqlUpdate += DataHelper.CRD_CREATE + " = '" + DataHelper.stringDate(this.createdDate) + "', ";
			if(lastUsedDate != null) sqlUpdate += DataHelper.CRD_LAST_USED + " = '" + DataHelper.stringDate(this.lastUsedDate) +  "', ";
			if(goBadTime != null) sqlUpdate += DataHelper.CRD_GO_BAD + " = '" + DataHelper.stringDate(this.goBadTime) + "', ";
			if(apiToken != null) sqlUpdate += DataHelper.CRD_API_TOKEN + " = '" + this.apiToken + "', ";
			if(refreshToken != null) sqlUpdate += DataHelper.CRD_REFRESH_TOKEN + " = '" + this.refreshToken + "', ";
			if(endUrl != null) sqlUpdate += DataHelper.CRD_END_URL + " = '" + this.endUrl + "', ";
			if(isSand) sqlUpdate += DataHelper.CRD_IS_SAND + " = '" + DataHelper.TRUE + "', ";
			else sqlUpdate += DataHelper.CRD_IS_SAND + " = '" + DataHelper.FALSE + "', ";
			sqlUpdate = sqlUpdate.substring(0, sqlUpdate.length() - 2);
			
			sqlUpdate += " WHERE " + DataHelper.ID_KEY + " = " + this.id;
			
			db.execSQL(sqlUpdate);
		}
	}
	
	public static Credential[] query(String whereClause, SQLiteDatabase db) {
		String selectSQL = "SELECT " + DataHelper.CRD_ALL + " FROM " + DataHelper.CRD_TB_NAME;
		if(whereClause != null) {
			if(!whereClause.startsWith(" "))
				whereClause = " " + whereClause;
			selectSQL += whereClause;
		}
		
		Log.d("Database","QUERY:\n" + selectSQL);
		Cursor c = db.rawQuery(selectSQL, null);
		Log.d("Database","QUERY ROWS: " + c.getCount());
		
		Credential[] queryList = new Credential[c.getCount()];
		if(c.getCount() > 0) {
			int i=0;
			c.moveToFirst();
			while(!c.isAfterLast()) {
				ArrayList<String> columns = new ArrayList<String>(Arrays.asList(c.getColumnNames()));
				String username = c.getString(c.getColumnIndex(DataHelper.CRD_USERNAME));
				String password = c.getString(c.getColumnIndex(DataHelper.CRD_PASSWORD));
				String authToken = c.getString(c.getColumnIndex(DataHelper.CRD_API_TOKEN));
				String refToken = c.getString(c.getColumnIndex(DataHelper.CRD_REFRESH_TOKEN));
				String endUrl = c.getString(c.getColumnIndex(DataHelper.CRD_END_URL));
				int id = c.getInt(c.getColumnIndex(DataHelper.ID_KEY));
				String token = null;
				if(columns.contains(DataHelper.CRD_TOKEN)) token = c.getString(c.getColumnIndex(DataHelper.CRD_TOKEN));
				Calendar lastUsed = null;
				if(columns.contains(DataHelper.CRD_LAST_USED)) lastUsed = DataHelper.parseDate(c.getString(c.getColumnIndex(DataHelper.CRD_LAST_USED)));
				Calendar created = null;
				if(columns.contains(DataHelper.CRD_CREATE)) created = DataHelper.parseDate(c.getString(c.getColumnIndex(DataHelper.CRD_CREATE)));
				Calendar goBad = null;
				if(columns.contains(DataHelper.CRD_GO_BAD)) goBad = DataHelper.parseDate(c.getString(c.getColumnIndex(DataHelper.CRD_GO_BAD)));
				boolean isSandbox = false;
				if(c.getString(c.getColumnIndex(DataHelper.CRD_IS_SAND)).equals(DataHelper.TRUE)) isSandbox = true;
				
				Log.d("Database","FOUND CREDENTIAL: {" + username + "|" + password + "|" + id + "|" + token + "|" + DataHelper.stringDate(lastUsed) + "|" + DataHelper.stringDate(created) + "}");
				Credential cred = new Credential(username, password);
				cred.id = String.valueOf(id);
				cred.securityToken = token;
				cred.lastUsedDate = lastUsed;
				cred.createdDate = created;
				cred.apiToken = authToken;
				cred.refreshToken = refToken;
				cred.endUrl = endUrl;
				cred.goBadTime = goBad;
				cred.isSand = isSandbox;
				queryList[i] = cred;
				i++;
				c.moveToNext();
			}
		}
		c.close();
		
		return queryList;
	}
	
	public int getId() {
		return Integer.valueOf(id);
	}
	
	public void delete(SQLiteDatabase db) {
		String deleteSql = "DELETE FROM " + DataHelper.CRD_TB_NAME + " WHERE " + DataHelper.ID_KEY + " = " + this.getId();
		db.execSQL(deleteSql);
	}
	
}
